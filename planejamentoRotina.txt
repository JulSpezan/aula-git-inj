
Planejamento de Rotina
Valor agregado (1 = maior prioridade)

Tarefas de Hoje:
1. Teminar primeira tarefa do GitLab e lançar
2. Fazer a tarefa de planejamento e lançar 
3. Responder o questionário de feedback
4. Rever a aula de HTML

Tarefas de Amanhã:
1. Fazer a tarefa de HTML e lançar (manhã; 8h-10h)
2. Assistir a primeira aula
3. Responder o primeiro questionário de feedback
4. Pausa/Almoço ---- Resolver problemas ocasionais 
5. Completar exercícios das aulas e responder feedback (16h-20h)


Rotina da semana (seg-sex):

MANHÃ:	
	 estudar matérias do curso (7h-10h)
	 aula1
 
TARDE:	
        almoço + descanso
	aula2		
	atividades das aulas(16h-20h);[ completar os exercícios propostos e enviar, estudar matérias vistas no dia]

NOITE:
        descanso
	revisar matérias
